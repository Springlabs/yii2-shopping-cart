<?php

namespace springdev\yii2\cart\interfaces;

/**
 * All objects that can be added to the cart must implement this interface
 *
 */
interface CartItemInterface {

    /**
     * Returns the price for the cart item
     *
     * @return int
     */
    public function getItemPrice(): float;

    /**
     * Returns the label for the cart item (displayed in cart etc)
     *
     * @return int|string
     */
    public function getItemLabel();

    /**
     * Returns the type for the cart item (displayed in cart etc)
     *
     * @return int|string
     */
    public function getItemType();

    /**
     * Returns the quantity for the cart item (displayed in cart etc)
     *
     * @return int|integer
     */
    public function getItemQuantity();

    /**
     * Returns unique id to associate cart item with product
     *
     * @return int|string
     */
    public function getItemUniqueId();
}
