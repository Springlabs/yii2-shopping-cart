<?php

namespace springdev\yii2\cart\interfaces;


/**
 * Interface StorageInterface
 *
 * @package yii2mod\cart\storage
 */
interface StorageInterface
{

    /**
     * @param Cart $cart
     */
    public function assign($user_id);
}