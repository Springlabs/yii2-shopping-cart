<?php

namespace springdev\yii2\cart;

use Yii;
use yii\base\Component;
use yii\base\InvalidParamException;
use springdev\yii2\cart\interfaces\CartItemInterface;
use springdev\yii2\cart\interfaces\StorageInterface;

/**
 * Class Cart provides basic cart functionality (adding, removing, clearing, listing items). You can extend this class and
 * override it in the application configuration to extend/customize the functionality
 *
 * @package yii2mod\cart
 */
class Cart extends Component {

    /**
     * @var string CartItemInterface class name
     */
    const ITEM_PRODUCT = '\springdev\yii2\cart\interfaces\CartItemInterface';

    /**
     * Override this to provide custom (e.g. database) storage for cart data
     *
     * @var string|\yii2mod\cart\storage\StorageInterface
     */
    public $storageClass = '\springdev\yii2\cart\storage\DatabaseStorage';

    /**
     * Enable cart variation 
     *
     * @var string|\yii2mod\cart\storage\StorageInterface
     */
    public $enableVariation = false;

    /**
     * @var array cart items
     */
    protected $items;

    /**
     * @var StorageInterface
     */
    private $_storage;

    /**
     * @inheritdoc
     */
    public function init() {
        $this->setStorage(Yii::createObject($this->storageClass, [$this->enableVariation]));
    }

    /**
     * Assigns cart to logged in user
     *
     * @param string
     * @param string
     */
    public function get($userId = null) {
        if (!is_null($userId)) {
            $storage = $this->getStorage();
            $storage->assign($userId);
            return $this;
        }
        return null;
    }

    /**
     * @return StorageInterface
     */
    public function getStorage(): StorageInterface {
        return $this->_storage;
    }

    /**
     * @param mixed $storage
     */
    public function setStorage($storage) {
        $this->_storage = $storage;
    }

    /**
     * Add an item to the cart
     *
     * @param models\CartItemInterface $element
     * @param bool $save
     *
     * @return $this
     */
    public function add(CartItemInterface $element, $variation_image = null, $variation_color = null) {
        return $this->storage->addItem($element, $variation_image, $variation_color);
    }

    /**
     * Add an item to the cart
     *
     * @param models\CartItemInterface $element
     * @param bool $save
     *
     * @return $this
     */
    public function addSubItem(CartItemInterface $element, $cart_item_id,$message = null) {
        return $this->storage->addSubItem($element, $cart_item_id,$message);
    }

    /**
     * Removes an item from the cart
     *
     * @param string $uniqueId
     * @param bool $save
     *
     * @throws \yii\base\InvalidParamException
     *
     * @return $this
     */
    public function remove($uniqueId) {
        return $this->storage->removeItem($uniqueId);
    }

    /**
     * Removes an item from the cart
     *
     * @param string $uniqueId
     * @param bool $save
     *
     * @throws \yii\base\InvalidParamException
     *
     * @return $this
     */
    public function removeSubitem($uniqueId) {
        return $this->storage->removeSubItem($uniqueId);
    }

    /**
     * Delete all items from the cart
     *
     * @param bool $save
     *
     * @return $this
     */
    public function clear() {
        return $this->storage->clear();
    }

    /**
     * @param string $itemType If specified, only items of that type will be counted
     *
     * @return int
     */
    public function getCount($itemType = null): int {
        return count($this->getItems($itemType));
    }

    /**
     * Returns all items of a given type from the cart
     *
     * @param string $itemType One of self::ITEM_ constants
     *
     * @return CartItemInterface[]
     */
    public function getItems($itemType = null): array {
        $items = $this->storage->getItems();
        if (!is_null($itemType)) {
            $items = array_filter(
                    $items, function ($item) use ($itemType) {
                /* @var $item CartItemInterface */
                return in_array($itemType, $item);
            }
            );
        }

        return $items;
    }

    /**
     * Returns all subItems for a Certain  item and with a given type
     *
     * @param string $itemType One of self::ITEM_ constants
     *
     * @return CartItemInterface[]
     */
    public function getSubItems($item_id, $itemType = null): array {
        $items = $this->storage->getSubItems($item_id);
        if (!is_null($itemType)) {
            $items = array_filter(
                    $items, function ($item) use ($itemType) {
                /* @var $item CartItemInterface */
                return in_array($itemType, $item);
            }
            );
        }

        return $items;
    }

    /**
     * Finds all items of type $itemType, sums the values of $attribute of all models and returns the sum.
     *
     * @param string $attribute
     * @param string|null $itemType
     *
     * @return int
     */
    public function getTotal($itemType = null): int {
        $sum = 0;
        if (!empty($this->getItems($itemType))) {
            foreach ($this->getItems($itemType) as $item) {
                if (isset($item['id']) && !empty($this->getSubItems($item['id']))) {
                    foreach ($this->getSubItems($item['id']) as $subitem) {
                        $sum += $subitem['price'];
                    }
                }
                $sum += $item['price'];
            }
        }

        return $sum;
    }

}
