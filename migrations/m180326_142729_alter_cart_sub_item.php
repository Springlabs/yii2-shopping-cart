<?php

use yii\db\Migration;

/**
 * Class m180326_142729_alter_cart_sub_item
 */
class m180326_142729_alter_cart_sub_item extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%cart_subitem}}', 'message', $this->text()->after('type'));

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
       $this->dropColumn('{{%cart_subitem}}', 'message');
    }

}
