<?php

use yii\db\Migration;

/**
 * Class m180312_144608_add_variation_to_cart_item
 */
class m180312_144608_add_variation_to_cart_item extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%cart_item}}', 'variation_image', $this->text()->after('price'));
        $this->addColumn('{{%cart_item}}', 'variation_color', $this->string()->after('variation_image'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
       $this->dropColumn('{{%cart_item}}', 'variation_image');
       $this->dropColumn('{{%cart_item}}', 'variation_color');
    }

}
