<?php

use yii\db\Migration;

/**
 * Class m171206_143609_add_cart_table
 */
class m171206_143609_add_cart_table extends Migration {

    /**
     * @inheritdoc
     */
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%cart}}', [
            'id' => $this->primaryKey(),
            'uid' => $this->string(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
                ], $tableOptions);

    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
         $this->dropTable('{{%cart}}');
    }

}
