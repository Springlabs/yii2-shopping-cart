<?php

use yii\db\Migration;

/**
 * Class m171206_151131_add_cart_sub_item_table
 */
class m171206_151131_add_cart_sub_item_table extends Migration {

    /**
     * @inheritdoc
     */
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%cart_subitem}}', [
            'id' => $this->primaryKey(),
            'item_id' => $this->integer(11)->notNull(),
            'subitem_id' => $this->integer(11)->notNull(),
            'label' => $this->string(),
            'price' => $this->float(),
            'type' => $this->string(),
            'quantity' => $this->integer(11),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
                ], $tableOptions);
        $this->addForeignKey('FK_cart_subitem_item_id', '{{%cart_subitem}}', 'item_id', '{{%cart_item}}', 'id', 'RESTRICT', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropTable('{{%cart_subitem}}');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m171206_151131_add_cart_sub_item_table cannot be reverted.\n";

      return false;
      }
     */
}
