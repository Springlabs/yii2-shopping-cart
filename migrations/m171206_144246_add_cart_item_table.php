<?php

use yii\db\Migration;

/**
 * Class m171206_144246_add_cart_item_table
 */
class m171206_144246_add_cart_item_table extends Migration {

    /**
     * @inheritdoc
     */
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%cart_item}}', [
            'id' => $this->primaryKey(),
            'cart_id' => $this->integer(11)->notNull(),
            'item_id' => $this->integer(11)->notNull(),
            'label' => $this->string(),
            'price' => $this->float(),
            'type' => $this->string(),
            'quantity' => $this->integer(11),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
                ], $tableOptions);
        $this->addForeignKey('FK_cart_item_cart_id', '{{%cart_item}}', 'cart_id', '{{%cart}}', 'id', 'RESTRICT', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        $this->dropTable('{{%cart_item}}');
    }

}
