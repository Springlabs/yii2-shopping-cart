<?php

namespace springdev\yii2\cart\storage;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\Object;
use springdev\yii2\cart\models\Cart;
use springdev\yii2\cart\models\CartItem;
use springdev\yii2\cart\models\CartSubitem;

/**
 * Class DatabaseStorage is a database adapter for cart data storage.
 *
 * If userComponent is set, it tries to call getId() from the component and use the result as user identifier. If it
 * fails, or if $userComponent is not set, it will use sessionId as user identifier
 *
 * @package yii2mod\cart\storage
 */
class DatabaseStorage extends Object implements \springdev\yii2\cart\interfaces\StorageInterface {

    private $_userid = null;
    private $_cartid = null;
    private $_enableVariation = false;

    public function __construct($enableVariation) {
        if ($enableVariation) {
            $this->_enableVariation = $enableVariation;
        }
    }

    /**
     * Create cart for user
     * @param type $user_id
     */
    public function assign($user_id) {
        $this->_userid = $user_id;
        $existing_cart = Cart::find()->where(['uid' => $user_id])->one();
        if (!$existing_cart) {
            $cart = new Cart();
            $cart->uid = $this->_userid;
            if ($cart->save()) {
                $this->_cartid = $cart->id;
            }
        } else {
            $this->_cartid = $existing_cart->id;
        }
    }

    /**
     * Add new item to cart
     * @param type $item
     */
    public function addItem($item, $variation_image = null, $variation_color = null) {
        $cart_item = new CartItem();
        $cart_item->cart_id = $this->_cartid;
        $cart_item->price = $item->getItemPrice();
        $cart_item->item_id = $item->getItemUniqueId();
        $cart_item->type = $item->getItemType();
        $cart_item->label = $item->getItemLabel();
        $cart_item->quantity = $item->getItemQuantity();
        if ($this->_enableVariation) {
            $cart_item->variation_image = $variation_image;
            $cart_item->variation_color = $variation_color;
        }
        if ($cart_item->save()) {
            return $cart_item;
        }
        return NULL;
    }

    /**
     * Add new item to cart
     * @param type $item
     */
    public function addSubItem($subitem, $item_id, $message) {
        $cart_sub_item = new CartSubitem();
        $cart_sub_item->item_id = $item_id;
        $cart_sub_item->subitem_id = $subitem->getItemUniqueId();
        $cart_sub_item->price = $subitem->getItemPrice();
        $cart_sub_item->type = $subitem->getItemType();
        $cart_sub_item->label = $subitem->getItemLabel();
        $cart_sub_item->message = $message;
        $cart_sub_item->quantity = $subitem->getItemQuantity();
        if ($cart_sub_item->save()) {
            return $cart_sub_item;
        }
        return NULL;
    }

    /**
     * Remove item from cart
     * @param type $item
     */
    public function removeItem($uniqueId) {
        $cart_item = CartItem::find()->where(['cart_id' => $this->_cartid, 'id' => $uniqueId])->one();
        if ($cart_item) {
            if ($cart_item->cartSubitems) {
                foreach ($cart_item->cartSubitems as $sub_item) {
                    $sub_item->delete();
                }
            }
            $cart_item->delete();
            return true;
        }
        return false;
    }

    /**
     * Remove item from cart
     * @param type $item
     */
    public function removeSubItem($uniqueId) {
        return CartItem::deleteAll(['id' => $uniqueId]);
    }

    /**
     * Clear all 
     * @param type $item
     */
    public function getItems() {
        return CartItem::find()->where(['cart_id' => $this->_cartid])->asArray()->all();
    }

    /**
     * Get items subitems
     * @param type $item
     */
    public function getSubItems($item_id) {
        return CartSubitem::find()->where(['item_id' => $item_id])->asArray()->all();
    }

    /**
     * Get items subitems
     * @param type $item
     */
    public function clear() {
        $cart = Cart::findOne(['id' => $this->_cartid]);
        if ($cart) {
            $items = $cart->cartItems;
            if ($items) {
                foreach ($items as $item) {
                    if ($item->cartSubitems) {
                        foreach ($item->cartSubitems as $sub_item) {
                            $sub_item->delete();
                        }
                    }
                    $item->delete();
                }
            }
            $cart->delete();
            return true;
        }
        return false;
    }

}
