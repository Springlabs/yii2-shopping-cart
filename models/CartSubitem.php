<?php

namespace springdev\yii2\cart\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "cart_subitem".
 *
 * @property integer $id
 * @property integer $item_id
 * @property integer $subitem_id
 * @property string $label
 * @property double $price
 * @property string $type
 * @property integer $quantity
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property CartItem $item
 */
class CartSubitem extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'cart_subitem';
    }

    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['item_id', 'subitem_id'], 'required'],
            [['item_id', 'subitem_id', 'quantity', 'created_at', 'updated_at'], 'integer'],
            [['price'], 'number'],
            [['label', 'type','message'], 'string', 'max' => 255],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => CartItem::className(), 'targetAttribute' => ['item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'item_id' => Yii::t('app', 'Item ID'),
            'subitem_id' => Yii::t('app', 'Subitem ID'),
            'label' => Yii::t('app', 'Label'),
            'price' => Yii::t('app', 'Price'),
            'type' => Yii::t('app', 'Type'),
            'quantity' => Yii::t('app', 'Quantity'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem() {
        return $this->hasOne(CartItem::className(), ['id' => 'item_id']);
    }

}
