<?php

namespace springdev\yii2\cart\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "cart_item".
 *
 * @property integer $id
 * @property integer $cart_id
 * @property integer $item_id
 * @property string $label
 * @property double $price
 * @property string $type
 * @property integer $quantity
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Cart $cart
 * @property CartSubitem[] $cartSubitems
 */
class CartItem extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'cart_item';
    }

    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['cart_id', 'item_id',], 'required'],
            [['cart_id', 'item_id', 'quantity', 'created_at', 'updated_at'], 'integer'],
            [['price'], 'number'],
            [['variation_image', 'variation_color'],'safe'],
            [['label', 'type'], 'string', 'max' => 255],
            [['cart_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cart::className(), 'targetAttribute' => ['cart_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'cart_id' => Yii::t('app', 'Cart ID'),
            'item_id' => Yii::t('app', 'Item ID'),
            'label' => Yii::t('app', 'Label'),
            'price' => Yii::t('app', 'Price'),
            'type' => Yii::t('app', 'Type'),
            'quantity' => Yii::t('app', 'Quantity'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCart() {
        return $this->hasOne(Cart::className(), ['id' => 'cart_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCartSubitems() {
        return $this->hasMany(CartSubitem::className(), ['item_id' => 'id']);
    }

}
